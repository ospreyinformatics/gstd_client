"""
exceptions.py
"""


class GstdError(Exception):
    """Base error class"""
    pass

class GstdBadPipelineDescriptionError(GstdError):
    """Status code: 2"""
    pass


class GstdCannotUpdateResourceError(GstdError):
    """Status code: 9"""
    pass


class GstdBadCommandError(GstdError):
    """Status code: 10"""
    pass


class GstdBadValueError(GstdError):
    """Status code: 13"""
    pass


class GstdEventError(GstdError):
    """Status code: 17"""
    pass


ERROR_CODE_MAP = {
    2: GstdBadPipelineDescriptionError,
    9: GstdCannotUpdateResourceError,
    10: GstdBadCommandError,
    13: GstdBadValueError,
    17: GstdEventError,
}