"""
data.py

Class for handling response data from the Gstreamer Daemon server.
"""

from collections import namedtuple

from gstd_client.exceptions import ERROR_CODE_MAP

BUS_MESSAGE_RESPONSE_KEYS = ('type', 'source', 'timestamp', 'seqnum', 'message', 'debug')

GstdNode = namedtuple('GstdNode', ['name'])  # a node representing a pipeline
GstdPipelineResponse = namedtuple('GstdResponse', ['nodes', 'properties'])
GstdBusResponse = namedtuple('GstdBusResponse', BUS_MESSAGE_RESPONSE_KEYS)
GstdProperty = namedtuple('GstdProperty', ['name', 'param_spec', 'value'])
GstdParamSpec = namedtuple('GstdParamSpec', ['access', 'blurb', 'construct', 'type'])


class GstdData(object):
    """Data structure for responses from the gstreamer daemon."""

    def __init__(self, json_data):
        self.code = json_data['code']
        self.description = json_data['description']
        if json_data.get('response'):
            self.response = json_data['response']
        self.success = self.code == 0

    @classmethod
    def parse_response(cls, response_data):
        """
        Create a GstdResponse data object from JSON data.

        Args:
            response_data (json): A JSON object with 'nodes' and 'properties' keys.
        """
        response = None
        if 'nodes' in response_data:
            props = [
                GstdProperty(
                    p['name'],
                    GstdParamSpec(
                        p['param_spec']['access'],
                        p['param_spec']['blurb'],
                        p['param_spec']['construct'],
                        p['param_spec']['type']),
                    p['value']
                ) for p in response_data.get('properties', [])
            ]
            nodes = [GstdNode(n['name']) for n in response_data.get('nodes', [])]
            response = GstdPipelineResponse(nodes, props)
        elif 'type' in response_data:
            response = GstdBusResponse(
                response_data.get('type'),
                response_data.get('source'),
                response_data.get('timestamp'),
                response_data.get('seqnum'),
                response_data.get('message'),
                response_data.get('debug'))
        return response
    
    def raise_for_status(self):
        """
        Raises an exception if the response code from
        gstreamer daemon is anything other than success.
        """
        if self.code in ERROR_CODE_MAP:
            raise ERROR_CODE_MAP[self.code](self.description)
        
