"""
client.py

Class for accessing the Gstreamer Daemon server.
"""

import json
import socket

from contextlib import contextmanager

from gstd_client.data import GstdData


class GstdClient(object):
    """Class for providing access to the gstreamer daemon server."""

    def __init__(self, server_ip='127.0.0.1', server_port=5000):
        """
        Instantiates the GstdClient object.

        Args:
            server_ip (str): The IP address on which the gstdaemon
            process is listening.
            server_port (int): Port on which the gstdaemon process
            is listening.
        """
        self.server_ip = server_ip
        self.server_port = server_port

    @contextmanager
    def _open_connection(self):
        """
        Context manager to open a socket connection to the gstdaemon server
        on enter and tear it down when the context exits.
        """
        sock = socket.create_connection((self.server_ip, self.server_port))
        yield sock
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()

    def _recv_data(self, sock, buffer_size=1024*1024):
        """
        Receive all available data on the socket and return it.

        Args:
            sock (socket): The socket on which we will receive data.
            buffer_size (int): The size of the buffer to read.
        """
        out_data = ''
        while True:
            recv_data = sock.recv(buffer_size)
            if not recv_data:
                break
            else:
                out_data += recv_data.decode('utf-8')
        return out_data

    def _parse_data(self, data):
        """
        Sanitizes and parses raw returned data.

        Args:
            data (str): The data to parse.
        """
        try:
            json_data = json.loads(data.rstrip(' \t\r\n\0'))
        except ValueError:
            raise ValueError('Gstreamer Daemon did not return valid json data')
        else:
            return GstdData(json_data)
    
    def cmd(self, cmd_name, *args):
        """
        Run a command on the gstreamer daemon server.

        Args:
            cmd_name (str): The command to run (e.g. pipeline_create).
            *args: The arguments to pass to the server along with the command.
        """
        with self._open_connection() as sock:
            cmd_str = '{} {}'.format(cmd_name, ' '.join(args))
            encoded_cmd = cmd_str.encode('utf-8')
            total_sent = 0
            while total_sent < len(encoded_cmd):
                sent = sock.send(encoded_cmd[total_sent:])
                if sent == 0:
                    raise RuntimeError('Socket connection broken')
                total_sent += sent
            return self._parse_data(self._recv_data(sock))

    def pipeline_create(self, name, description):
        """
        Create a gstreamer pipeline.

        Args:
            name (str): The name of the pipeline.
            description (str): The gstreamer pipeline definition to implement
            in gst-launch syntax (e.g. "videotestsrc name=vts ! autovideosink")
        """
        return self.cmd('pipeline_create', name, description)

    def pipeline_delete(self, name):
        """
        Deletes the pipeline with the given name.

        Args:
            name (str): The name of the pipeline.
        """
        return self.cmd('pipeline_delete', name)

    def pipeline_play(self, name):
        """
        Play a pipeline.

        Args:
            name (str): The name of the pipeline.
        """
        return self.cmd('pipeline_play', name)

    def pipeline_pause(self, name):
        """
        Pause a pipeline.

        Args:
            name (str): The name of the pipeline.
        """
        return self.cmd('pipeline_pause', name)

    def pipeline_stop(self, name):
        """
        Stop a pipeline.

        Args:
            name (str): The name of the pipeline.
        """
        return self.cmd('pipeline_stop', name)

    def element_set(self, pipe, element, property, value):
        """
        Sets a property in an element of a given pipeline.

        Args:
            pipe (str): name of the pipeline.
            element (str): name of the element in the pipeline.
            property (str): property of the element to set.
            value (str): value to set.
        """
        return self.cmd('element_set', pipe, element, property, value)
    
    def element_get(self, pipe, element, property):
        """
        Queries a property in an element of a given pipeline.

        Args:
            pipe (str): name of the pipeline.
            element (str): name of the element in the pipeline.
            property (str): property of the element to get.
        """
        return self.cmd('element_get', pipe, element, property)
    
    def list_pipelines(self):
        """
        List the existing pipelines.
        """
        return self.cmd('list_pipelines')
    
    def list_elements(self, pipe):
        """
        List the elements in a given pipeline.

        Args:
            pipe (str): Name of the pipeline.
        """
        return self.cmd('list_elements', pipe)
    
    def list_properties(self, pipe, element):
        """
        List the properties of an element in a given pipeline.

        Args:
            pipe (str): Name of the pipeline.
            element (str): Name of the element.
        """
        return self.cmd('list_properties', pipe, element)
    
    def bus_read(self, pipe):
        """
        Read the bus messages in the pipeline.
        
        Args:
            pipe (str): Name of the pipeline.
        """
        return self.cmd('bus_read', pipe)
    
    def bus_filter(self, pipe, filter_name):
        """
        Select the types of message to be read from the bus.

        Args:
            pipe (str): Name of the pipeline.
            filter_name (str): Filter(s) to apply (separate multiple with '+',
            i.e.: eos+warning+error.)
        """
        return self.cmd('bus_filter', pipe, filter_name)
    
    def bus_timeout(self, pipe, time_ns):
        """
        Apply a timeout for the bus polling.
        
        Args:
            pipe (str): Name of the pipeline.
            time_ns (int): Time to wait in nanoseconds. -1: forever, 0: return immediately.
        """
        return self.cmd('bus_timeout', pipe, str(time_ns))

    def event_eos(self, pipe):
        """
        Send an end-of-stream event.

        Args:
            pipe (str): Name of the pipeline.
        """
        return self.cmd('event_eos', pipe)
    
    def event_seek(self, pipe, rate=1.0, format=3, flags=1, start_type=1, start=0, end_type=1, end=-1):
        """
        Perform a seek in the given pipeline.

        Args:
            pipe (str): Name of the pipeline.
            rate (float): The new playback rate. Default value: 1.0.
            format (int): The format of the seek values. Default value: 3.
            flags (int): The optional seek flags. Default value: 1.
            start_type (int): The type and flags for the new start position. Default value: 1.
            start (int): The value of the new start position. Default value: 0.
            end_type (int): The type and flags for the new end position. Default value: 1.
            end (int): The value of the new end position. Default value: -1.
        """
        return self.cmd('event_seek', pipe, rate, str(format), str(flags), str(start_type), str(start), str(end_type), str(end))

    def event_flush_start(self, pipe):
        """
        Put the pipeline in flushing mode.

        Args:
            pipe (str): Name of the pipeline.
        """
        return self.cmd('event_flush_start', pipe)

    def event_flush_stop(self, pipe, reset=True):
        """
        Take the pipeline out from flushing mode.
        
        Args:
            pipe (str): Name of the pipeline.
            reset (bool): If time should be reset.
        """
        return self.cmd('event_flush_stop', pipe, str(reset).lower())

    def debug_enable(self, enable):
        """
        Enable/Disable GStreamer debug.

        Args:
            enable (bool): Whether to enable debug.
        """
        return self.cmd('debug_enable', str(enable).lower())
    
    def debug_threshold(self, threshold):
        """
        The debug filter to apply (as you would use with gst-launch).

        Args:
            debug_threshold (int): value of the level of debug.
        """
        return self.cmd('debug_threshold', str(threshold))

    def debug_color(self, enable):
        """
        Enable/Disable colors in the debug logging.

        Args:
            enable (bool): Whether to enable colors in debug logs.
        """
        return self.cmd('debug_color', str(enable).lower())
