# GStreamer Daemon Client Library #

This library provides a python TCP client for the [GStreamer Daemon framework](https://github.com/RidgeRun/gstd-1.x).

## Dependencies ##

* Python 3.6

## Usage ##

With GStreamer Daemon installed and the gstd process running on your system (or a remote system, if you so choose).

```python
from gstd_client import GstdClient
from gstd_client.exceptions import GstdError

GSTD_IP = '127.0.0.1'
GSTD_PORT = 5000

# When instantiating the client, IP defaults to '127.0.0.1'
# and port defaults to 5000, so you can leave these values
# out if using the defaults.
client = GstdClient(GSTD_IP, GSTD_PORT)

# Create a new pipeline
data = client.pipeline_create('p1', 'videotestsrc name=vts ! autovideosink')
try:
    # Raise an exception if anything other than success was returned.
    data.raise_for_status()
except GstdError as e:
    # Do something useful here.
    print("GStreamer Daemon returned an error: {}".format(str(e)))
else:
    # The returned GstdData object can be examined for detailed response
    # properties. E.g. data.response includes multiple properties. The response
    # spec can be seen here:
    # https://developer.ridgerun.com/wiki/index.php?title=GStreamer_Daemon_-_Response_Format
    print(data.description)

# The full High Level API has been implemented on the GstdClient class.
# Find the complete list of API calls here:
# https://developer.ridgerun.com/wiki/index.php?title=GStreamer_Daemon_-_API_Reference

# Play the new pipeline.
data = client.pipeline_play('p1')
try:
    data.raise_for_status()
except GstdError as e:
    print(str(e))
else:
    print(data.description)
```
